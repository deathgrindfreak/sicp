(declare (unit sets))

(define (element-of? x set)
  (cond ((null? set) #f)
        ((equal? x (car set)) #t)
        (else (element-of? x (cdr set)))))

(define (adjoin-set x set)
  (if (element-of? x set)
      set
      (cons x set)))

(define (intersection set1 set2)
  (cond ((or (null? set1) (null? set2)) '())
        ((element-of? (car set1) set2)
         (cons (car set1)
               (intersection (cdr set1) set2)))
        (else (intersection (cdr set1) set2))))

;; Duplicate implemenation
(define (element-of? x set)
  (cond ((null? set) #f)
        ((equal? x (car set)) #t)
        (else (element-of? x (cdr set)))))

(define (adjoin-set x set) (cons x set))

(define (intersection set1 set2)
  (cond ((or (null? set1) (null? set2)) '())
        ((element-of? (car set1) set2)
         (cons (car set1)
               (intersection (cdr set1) set2)))
        (else (intersection (cdr set1) set2))))

;; Sorted list
(define (adjoin-set x set)
  (cond ((null? set) '(x))
        ((>= x (car set)) (cons x set))
        (else (cons (car set)
                    (adjoin-set x (cdr set))))))

(define (union set1 set2)
  (cond ((null? set1) set2)
        ((null? set2) set1)
        ((let ((x1 (car set1)) (x2 (car set2)))
           (cond ((= x1 x2) (cons x1 (union (cdr set1) (cdr set2))))
                 ((> x1 x2) (cons x2 (union set1 (cdr set2))))
                 ((< x1 x2) (cons x1 (union (cdr set1) set2))))))))
